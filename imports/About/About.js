import React from 'react'


export default class About extends React.Component  {
     
    render () {

        return (
            <div>
               <div className = "backgroundimg"> 
                   <h1 className = "title"> About </h1>
               </div>
               <div className = "section">
                     <h1>THE STORY BEHIND OUR STORE</h1>
                    <div className = "sectionone">
                            <div className = "descriptionone">
                                
                                <p>Founded in 1948 by Cino Cinelli, an Italian road cyclist who became a professional racer between 1937 to 1944. Cinelli won the Milan–San Remo in 1943, the Giro di Lombardia in 1938, and the Tour of the Apennines in 1937.</p>
                                <p>Cino became interested in bicycle technology after experiencing mechanical failures during races. He began developing innovative new product designs, but failed to gain manufacturer support from others in the bike industry. So Cino partnered with his brother Giotto, who was producing steel stems and bars in Florence. He soon moved the company to Milan, which had become the center of the Italian cycling industry…and art.</p>
                                <p>In 1978, Antonio Colombo, owner of the world-renowned Columbus tubing, assumed presidency of the company, and in 1997 Cinelli became a division of Gruppo S.r.l. The company is still—to this day—run very much like a family business. And like all lovers of cycling, Antonio still very much enjoys riding bikes.</p>
                            </div>
                            <div className = "thumbone"> <img src="https://res.cloudinary.com/dgna411zy/image/upload/v1530788901/riders_8.jpg" /></div>
                    </div>
                    <div className ="sectiontwo">
                            <div className = "thumbtwo"> <img src="https://res.cloudinary.com/ssoutoul/image/upload/v1530523059/riders_2-1024x768.jpg" /></div>
                            <div className = "descriptiontwo">
                                <p>Founded in 1948 by Cino Cinelli, an Italian road cyclist who became a professional racer between 1937 to 1944. Cinelli won the Milan–San Remo in 1943, the Giro di Lombardia in 1938, and the Tour of the Apennines in 1937.</p>
                                <p>Cino became interested in bicycle technology after experiencing mechanical failures during races. He began developing innovative new product designs, but failed to gain manufacturer support from others in the bike industry. So Cino partnered with his brother Giotto, who was producing steel stems and bars in Florence. He soon moved the company to Milan, which had become the center of the Italian cycling industry…and art.</p>
                                <p>In 1978, Antonio Colombo, owner of the world-renowned Columbus tubing, assumed presidency of the company, and in 1997 Cinelli became a division of Gruppo S.r.l. The company is still—to this day—run very much like a family business. And like all lovers of cycling, Antonio still very much enjoys riding bikes.</p>
                            </div>
                    </div>
               </div>
               <div className = "sectionthree">
                    <h1 style = {{textAlign: "center",padding: "20px"}}>Our Team </h1>
                    <div className ="gallery">
                        <div>
                            <img className = "thumb"src="https://res.cloudinary.com/ssoutoul/image/upload/v1530522926/G14-headshot-sm-1024x1024.jpg"/>
                            <p>Gracie Elvin</p>
                        </div>
                        <div>
                            <img className = "thumb" src="https://res.cloudinary.com/ssoutoul/image/upload/v1530522627/TedKing-headshot.png"/>
                            <p>Maxime Smith</p>
                        </div>
                        <div>
                            <img className = "thumb" src="https://res.cloudinary.com/ssoutoul/image/upload/v1530522710/images.jpg"/>
                            <p>Kyle Borch</p>
                        </div>
                        <div>
                            <img className = "thumb" src="https://res.cloudinary.com/ssoutoul/image/upload/v1530522704/Frank-Headshot-1-e1515563134911.jpg"/>
                            <p>Steven Smith</p>
                        </div>
                    </div>
               </div>
                
                
                
            </div>
        )
    }
    
    
}


