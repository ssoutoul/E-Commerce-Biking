import React from 'react'


class Message extends React.Component{
    render() {
             let visible = {
                      background:this.props.color,
                      color:this.props.colorText,
                      maxWidth:'366px',
                      margin:'0 auto',
                      padding:'.5rem',
                      borderRadius:'5px',
                      marginBottom:this.props.marginBottom,
                      textAlign:'center',
                      display:this.props.display
              }

              

        return (
            <div className="message"
                style = {visible}>
              <p>{this.props.message}</p>
            </div>
          )
                
            }

        }

      
    


export default Message
