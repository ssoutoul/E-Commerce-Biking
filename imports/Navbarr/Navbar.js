import React from 'react'
import { NavLink } from 'react-router-dom'
import Home from '../Home/Home'
import About from '../About/About'
import Shop from '../Shop/Shop'
import Contact from '../Contact/Contact'
import Cart from '../Cart/Cart'


export default class Navbar extends React.Component {

    render () {
        let activeStyle = {
            color:'#00a795'
            
        }

        
        return (
           <ul className = 'theUL'>
               <li className = 'white'>
                   <NavLink to= "/" activeStyle = {activeStyle}>
                   <img className = 'img' src='http://res.cloudinary.com/ssoutoul/image/upload/v1530104876/logo.png'/>
                   </NavLink>
               </li>
               <li>
                   <NavLink to = "/shop" activeStyle = {activeStyle} className = 'navwords'>
                        Shop
                   </NavLink>
               </li>
               <li>
                   <NavLink to = "/about"  activeStyle = {activeStyle} className = 'navwords'>
                       About
                   </NavLink>
               </li>
               <li>
                   <NavLink to = "/contact"  activeStyle = {activeStyle} className = 'navwords'>
                        Contact
                   </NavLink>
               </li>
               <li>
                   <NavLink to ='/cart' activeStyle = {activeStyle} className = 'cart'>
                           <i className="fas fa-shopping-cart"></i>
                           <span style = {{color: "#00A795", marginLeft: '2%'}}>{this.props.cart}</span>
                   </NavLink>
               </li>
           </ul> 
        )
    }
}
