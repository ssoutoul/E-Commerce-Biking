import React from "react";
import { NavLink } from 'react-router-dom';
import Home from '../Home/Home';
import About from '../About/About';
import Shop from '../Shop/Shop';
import Contact from '../Contact/Contact';
import Cart from '../Cart/Cart';

export default ({ close }) => (
  <div className="menu">
    <ul>
      <li>
        <NavLink onClick={close} activeClassName="current" to="/">
          Home
        </NavLink>
      </li>
      <li>
       <NavLink onClick={close} activeClassName="current" to="/shop">
                         Shop                   
       </NavLink>
     </li>
      <li>
        <NavLink onClick={close} activeClassName="current" to="/about">
          About
        </NavLink>
      </li>
      <li>
        <NavLink onClick={close} activeClassName="current" to="/contact">
          Contact
        </NavLink>
      </li>
      <li>
       <NavLink  onClick={close} activeClassName="current"  to = "/cart">
          <i className="fas fa-shopping-cart"></i>
       </NavLink>
     </li>
    </ul>
  </div>
);
