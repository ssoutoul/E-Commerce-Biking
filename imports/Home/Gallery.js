import React from 'react';
import "react-alice-carousel/lib/alice-carousel.css";
import AliceCarousel from 'react-alice-carousel';
import Button from '@material-ui/core/Button';
import { Products_api }   from '../api/products_api';
import { DialogContent } from '@material-ui/core';
import { NavLink } from 'react-router-dom';
 
export default class Gallery extends React.Component  {

  componentDidMount(){
    var arr = []
    Tracker.autorun(()=>{
      var x = Products_api.find({}).fetch();
      x.map((ele,i)=>{
        if(i < 3){
          arr.push(ele)
        }
      })
    this.setState({arr},()=>{
      console.log(arr)
    })
    })
  }
  state ={
    name : '',
    description:'',
    arr:[]

  }

  shortener = str => str.substr(0,600) + '...'
  
  render(){
    function createMarkup(stuff) { return {__html:stuff}; };
    if(this.state.arr.length > 0){
  return(

    <AliceCarousel>
    {
      this.state.arr.map((ele,i)=>{
    return<div className = 'steven' > 
    <div className = 'littlegrid' key ={i}>
    <NavLink to= {"/product/" + ele._id}>
    <img src={ele.url} className="miniShop" />
    </NavLink>
   <div>
   <NavLink to= {"/product/" + ele._id}>
    <p className ='classofp' >{ele.title}</p>
    </NavLink>
    <NavLink to= {"/product/" + ele._id}>
    <div className = "productdescription" dangerouslySetInnerHTML={createMarkup(this.shortener(ele.description))} />
    </NavLink>
    {/* <p className ='classofp' >{ele.description}</p> */}
    <NavLink to= {"/product/" + ele._id}>
    <Button variant="contained" color="secondary" style = {{background: "#00A795", width: "50%",marginLeft: "49%"}}>
        Shop
      </Button>
    </NavLink>
    </div>
    </div>
    </div>
 
    })
  }
    </AliceCarousel>
)
    }else return <h1 className = 'load'>Loading...</h1>
  }}
