import React from 'react'
import BackgroundSlideShow from './BackgroundSlideshow'
import Gallery from './Gallery';
import Brands from './Brands';


    
    
    export default () => {
        return (
            <div>
                
                <BackgroundSlideShow />
                <Gallery/>
                <Brands/>

            </div>
        )
    }

  
   