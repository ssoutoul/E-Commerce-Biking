import React from 'react';
import { Slide } from 'react-slideshow-image';


const images = [
  'https://res.cloudinary.com/ssoutoul/image/upload/v1530198250/brandon_semenuk_rampage_2015.jpg',
  'https://res.cloudinary.com/ssoutoul/image/upload/v1530197737/mountainbiken-downhill.jpg',
  'https://res.cloudinary.com/ssoutoul/image/upload/v1530198098/mountain-biking.jpg'
];

const BackgroundSlideShow = () => {
    return (
        <Slide
          images= {images}
          duration={3000}
          transitionDuration={1000}
        />
    )
}

export default  BackgroundSlideShow