import React from 'react'
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom'

import Navbar from './Navbarr/Navbar'
import Shop from './Shop/Shop'
import Products from './Products/Products'
import About from './About/About'
import Contact from './Contact/Contact'
import Home from './Home/Home'
import Cart from './Cart/Cart'
import Footer from './Footer'
import Popup from "reactjs-popup";
import Hamburger from "./Navbarr/Hamburger";
import Menu from "./Navbarr/Menu";
import Admin from "./Admin/Admin"
import AdminAccess from './Admin/AdminAccess'
import Checkout from './Checkout/Checkout'
import { Products_api }   from './api/products_api'
import { Cart_api }   from './api/cart_api'
import Message from './Message'
import Modal from 'react-responsive-modal';

// import NavBarNPM from 'reactjs-navigation'

import Gallery from './Home/Gallery'
import Success from './Checkout/Success';

export default class App extends React.Component {

  state = {
    product: '', selected: '', total: '', open: false, text: '', backgroundColor: ''
  }

  componentDidMount(){
    this.findStuff()
  }
  findStuff  = () => {
    var that = this
    Tracker.autorun(()=>{
      var apple = 0
      var cart = [];
      cart = Cart_api.find({}).fetch()
      cart.map((ele) => {
   
        apple += ele.qty 
      })      
      that.setState({cart:apple},()=>{
      
      })
      
    })
  }
  remove = id => {
  
    Cart_api.remove({_id:id}, (e,d) => {
   
      this.findStuff()
    })

  }

  collectSize = (size) => {
     
      this.setState({selected: size})
    

  }

  onCloseModal = () => {
    
    this.setState({ open: false });
  };

  onOpenModal = () => {
    this.setState({ open: true, text: "You must select a size" });
  };

  insertCart = (id,size) => {
    
    var product = {
        qty:1,
        productID:id,
        selected: this.state.selected
    }

 

    // check if the product is inside the cart_api using productID
    // var x =Cart_api.findOne({productID:id})
    var hello = Cart_api.findOne({productID:id, selected: this.state.selected})
    debugger
    
    var y = this.state.selected
  
    
    if(hello) {
        Cart_api.update({productID:id, selected: this.state.selected},{$inc: {qty:1}}, (e,d)=> {
          debugger
              if(e) {
                this.setState({ open: true, text: 'Not successfully added to the cart'});
               
              } else {
                debugger
                this.setState({ open: true, text: 'Product added again, you have now ' + (hello.qty+1) + ' times this product', backgroundColor: 'green'});
               
            }
        })
      } else {
          Cart_api.insert(product,(e,d)=>{
            
            if (e) {
              this.setState({ open: true, text: 'Not successfully added to the cart'});
            } else {
                this.setState({ open: true, text: 'Successfully added to the cart', backgroundColor: 'green'});
              
            }
        })
      }
        
    }

  _renderProduct = (id) => {
     Tracker.autorun(() => {
      var  x =  Products_api.findOne({_id:id})
      this.setState({product: x})
     })
    
     
  }

  increaseQuantity = (id,size) => {
  
      Cart_api.update({_id:id},{$inc: {qty:1}}, (e,d)=> {
          
          
      }) 

  }  

  decreaseQuantity = (id) => {
    var cart = Cart_api.findOne({_id:id})
    debugger
    if(cart.qty < 2){
      
      return null
      // alert('cannot go less than 0!')
    }
     Cart_api.update({_id:id},{$inc: {qty:-1}}, (e,d)=> {
          
     }) 
   }  



  render(){

    let styles = {
      fontFamily: "sans-serif",
      textAlign: "center",
      marginTop: "40px"
    }

    let contentStyle = {
      background: "rgba(255,255,255,0)",
      width: "80%",
      border: "none"
    }
    
    let style ={ display:this.state.display || 'none' }

    return(
          

   
   
          <Router>
            <div>
              
                <Popup
                    modal
                    overlayStyle={{ background: "rgba(255,255,255,0.98" }}
                    contentStyle={contentStyle}
                    closeOnDocumentClick={false}
                    trigger={open => <Hamburger open={open} />}
                >
                {close => <Menu close={close} />}
              </Popup>
               <Navbar cart = {this.state.cart}/>
                    <Route exact path = "/" component = {Home} />
                    <Route path = '/shop' render  = {(props)=>(
                        <Shop
                           insertCart = {this.insertCart}
                           size = {this.collectSize}
                           open = {this.state.open}
                           text = {this.state.text}
                           color= {this.state.backgroundColor}
                           close = {this.onCloseModal}
                           onOpenModal = {this.onOpenModal}
                           
                        />
                        
                    )}/>
                    <Route path="/product/:id" render = {(props)=> (
                      <Products
                        {...props}
                        _renderProduct = {this._renderProduct}
                        product = {this.state.product}
                        insertCart = {this.insertCart}
                        size = {this.collectSize}
                        open = {this.state.open}
                        text = {this.state.text}
                        color= {this.state.backgroundColor}
                        close = {this.onCloseModal}
                        onOpenModal = {this.onOpenModal}
                       
                      />
                    )}
                        
                    />
                    <Route path = '/about' component = {About}/>
                    <Route path = '/contact' component = {Contact}/>
                    <Route path = '/login' component = {Admin}/>
                    <Route path = '/admin' component = {AdminAccess}/>
                    <Route path = '/cart' render = {(props) => (
                      <Cart
                        remove = {this.remove}
                        open = {this.state.open}
                        text = {this.state.text}
                        close = {this.onCloseModal}
                        increaseQuantity = {this.increaseQuantity}
                        decreaseQuantity = {this.decreaseQuantity}
                        
                      />
                    )}/>
                    <Route path='/checkout' component = {Checkout}/>
                    <Route path = '/success' component = {Success}/>
                <Footer />
            </div>
          </Router>
      )
  }
}