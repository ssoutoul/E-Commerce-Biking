import React from 'react'
import { Button } from '@material-ui/core';
import { NavLink } from 'react-router-dom'

// import Logo from '../imports/images/logo.png'

export default class Footer extends React.Component {
   
    render () {

       let activeStyle = {
        color:'#00a795'
        }

       

        let icon = {
            color: "white",
            fontSize: "40px",
            marginRight: "10px"
            
        }
    

        return (
            <div className= "footer">
                <div>
                <NavLink to= "/" activeStyle = {activeStyle}>
                    <img className = "logostyle" src= "http://res.cloudinary.com/ssoutoul/image/upload/v1530104876/logo.png"/>
                </NavLink>
                </div>
                <div>
                    <h1 className = "text" >WE’RE AVAILABLE</h1>
                    <h1 className = "text"  >MONDAY- FRIDAY 8:30 A.M. - 5:30 P.M.</h1>
                    <NavLink to = "/contact">
                    <h1 className = "text"  ><i style = {icon} className="fas fa-map-marked-alt"></i>Gran Via Les Corts Catalana, 395</h1>
                    </NavLink>
                    <h1 className = "text"  >08015, Barcelona</h1>
                    <NavLink to = "/contact">
                    <h1 className = "text"  ><i style = {icon} className="fas fa-phone"></i>930 15 32 97</h1>
                    </NavLink>
                    <NavLink to = "/contact">    
                    <h1 className = "text"  ><i  style = {icon} className="fas fa-envelope"></i>ridersboutiquebarcelona@gmail.com</h1>
                    </NavLink>
                </div>
                <div>
                <iframe className='themap' src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2993.8753939563358!2d2.1487947154254514!3d41.37679187926506!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4a3202d6aeda5%3A0x92d1214178c0904c!2sRider+Boutique+Barcelona!5e0!3m2!1sen!2ses!4v1530453359970"  frameBorder="0" style= {{border:0}} allowFullScreen></iframe>                </div>
                <div>
                    <h1 className = "follow" > Follow Us</h1>
                    <div className = "icongrid"> 
                        <a 
                        style = {{color: "white", fontSize: "50px",backgroundColor: 'black',border:'0px'}}  className="fab fa-instagram"
                        href ="https://www.instagram.com/ridersboutique.barcelona/?hl=en"></a>
                        <a 
                        style = {{color: "white", fontSize: "50px",backgroundColor: 'black',border:'0px'}}  className="fab fa-facebook"
                        href ="https://www.facebook.com/ridersboutiquebarcelona/"></a>
                        
                    </div>
                </div>
            </div>   
        )
    }
   


}