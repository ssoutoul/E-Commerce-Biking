import React from 'react'
import Shipping from'./Shipping'
import {Cart_api} from '../api/cart_api'
import { Products_api } from '../api/products_api';
import  Message from '../Message'


export default class Checkout extends React.Component {
    state = {
        order: '', orders : [] , arr:[]
    }
    componentDidMount () {
        var arr = []
        Tracker.autorun(() => {
            arr = []
            var orders = Cart_api.find({}).fetch()
            
            orders.map((ele)=>{
               
                var myProduct = Products_api.findOne({_id:ele.productID})
                
                if(myProduct){
                    myProduct.qty = ele.qty
                    myProduct.order_id = ele._id
                    myProduct.selected = ele.selected
                    arr.push(myProduct)
                }
                
            })
        this.setState({arr},()=>{
            debugger
            console.log('-===-',this.state)
        })
       
    })
}
    render () {

        return (
        <div>
            <h1 style = {{color:'white'}}>Shipping Address</h1>
           

        
            <Shipping
                orders = {this.state.arr}
                history = {this.props.history}
                
            />
          

            
        </div>
        )


        
    }
}