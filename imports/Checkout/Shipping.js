import React from 'react';
import { Button } from '@material-ui/core';
import Payment from './Payment';

export default class Shipping extends React.Component{

    state = {
        Firstname : '',
        Lastname : '',
        AddressLine1 :'', 
        AddressLine2 :'',
        Postcode:'',
        City:'',
        Phonenumber:'',
        Email:''
    }

    

    handleChange= (event) =>{
        var cb = () => console.log(this.state)
        this.setState({[event.target.name]: event.target.value},cb)
    }
    
_renderTotal = () => {
    var total = 0
    this.props.orders.map ((el,i)=>{
        
        el.index = i
        total += (Number(el.price) * el.qty)
         
     }) 
    
     return total
}

    render(){

        return(
    <div>
<form 
onChange={this.handleChange}
>


<h1>Shipping Addresss</h1>
<div className = 'toGrid'>
<div className = 'mysalsa'>
{/* ........... */}
<div>
<span className='forsize'>First Name</span>
</div>
<div>
<div>
    <input 
      required
      className = 'makeitbig'
      type='text' 
      name='Firstname' 
      autoComplete="off"
      placeholder ='Firstame' 
      value = {this.state.Firstname}
    />
</div>
</div>
{/* .............. */}
<div>
<span className='forsize'>Last Name</span>
</div>
<div>
<div>
    <input
        required
     className = 'makeitbig'
     type='text' name='Lastname' 
      autoComplete="off"
      placeholder ='Lastname' 
      value = {this.state.Lastname}
    />
</div>
</div>
{/* ............ */}
<div>
<span className='forsize'>Address Line 1 </span>
</div>
<div>
<div>
<input
     
     required
     className = 'makeitbig'
     type='text' name='AddressLine1' 
      autoComplete="off"
      placeholder ='ex. Oxford street ' 
      value = {this.state.AddressLine1}
    />
</div>
</div>
{/* ............. */}
<div>
<span className='forsize'>Address Line 2 </span>
</div>
<div>
<div>
<input
     
     required
     className = 'makeitbig'
     type='text' name='AddressLine2' 
      autoComplete="off"
      placeholder ='ex. House number:22/Floor 3 Ap. No:40' 
      value = {this.state.AddressLine2}
    />
</div>
</div>
{/* .............. */}
<div>
<span className='forsize'>Postcode</span>
</div>
<div>
<div>
<input
     
     required
     className = 'makeitbig'
     type='text' name='Postcode' 
      autoComplete="off"
      placeholder ='Postcode' 
      value = {this.state.Postcode}
    />
</div>
</div>
{/* ............... */}
<div>
<span className='forsize'>City</span>
</div>
<div>
<div>
<input
     
     required
     className = 'makeitbig'
     type='text' name='City' 
      autoComplete="off"
      placeholder ='Ex. Barcelona' 
      value = {this.state.City}
    />
</div>
</div>
{/* ............... */}
<div>
<span className='forsize'>Phone number</span>
</div>
<div>
<div>
<input
    
    required
     className = 'makeitbig'
     type='number' name='Phonenumber' 
      autoComplete="off"
      placeholder ='Ex.+1234567890' 
      value = {this.state.Phonenumber}
    />
</div>
</div>
{/* ........... */}
<div>
<span className='forsize'>E-mail</span>
</div>
<div>
<div>
<input
    
    required
     className = 'makeitbig'
     type='email' name='Email' 
      autoComplete="off"
      placeholder ='Ex. example@example.com' 
      value = {this.state.Email}
    />
</div>

</div>
{/* ........... */}

</div>
<div className = 'theboss'>
<div className = 'trevor' style = {{width:'70%'}}>
<div className = 'theline' >
<span className = 'bob'>Order Total</span>
<span className = 'bob'>{this._renderTotal()} €</span>
</div>
</div>
</div>
<div className = 'vice'>


</div>
</div>
</form>
<Payment
  amount= {this._renderTotal()}

  history = {this.props.history}
  orders = {this.props.orders}
  email = {this.state.Email}
  payment = {this.state}
  {...this.state}

  
/>
</div>
        )
}
}