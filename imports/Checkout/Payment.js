import StripeCheckout from 'react-stripe-checkout';
import { secret } from '../../secret'
import React from 'react'
import Success from './Success'
import  Message from '../Message'
import { Button } from '@material-ui/core';
import { Cart_api }   from '../api/cart_api';


export default class Payment extends React.Component{
    state = {
        color:'', message: '', display: '', colorText: '', marginBottom:'', displayPayment: 'none', displayButton: 'block'
    }

    paymentSubmit = e => {
        
        var self = this
        var count = 0
       e.preventDefault()
       debugger
       for (var key in this.props.payment) {
            if(!this.props.payment[key] == '' ) {
                debugger
                count++
    
                
            } else {
                debugger

                this.setState({
                    color:'#f8d7da',
                    message:'You need to fill out all the fields',
                    display:'block',
                    colorText: "#721c24",
                    marginBottom: "30px"
                },()=>{
                    setTimeout(()=>{
                      this.setState({display:'none'})
                    },5000)
                })
                
                
            }

            if (count>7) {
                debugger
                // this.setState({display:'none'})
                this.setState({displayPayment:'block'})
                this.setState({displayButton:'none'})
                
            } 
        } 
       
    } 


    
    onToken =  (token) => {
      var that  = this

        
        Meteor.call(
            'StripeChargeMethod',
            token, 

          
            (this.props.amount * 100),

            (err,data)=>{ 
                
               if(err){

                    this.setState({
                        color:'#f8d7da',
                        message:'Payment did not go through. Please try again',
                        display:'block',
                        colorText: "#721c24",
                        marginBottom: "30px"
                    },()=>{
                        setTimeout(()=>{
                          this.setState({display:'none'})
                        },7000)
                    })
               }else if(data){
               // to check the data
              if(data.status == "succeeded"){
                    
                    let  {
                        Firstname,
                        Lastname,
                        AddressLine1 ,
                        AddressLine2,
                        Postcode,
                        City,
                        Phonenumber,
                        Email,
                    } = this.props

                    let email = {
                        receiver: Email,
                        receiver2: "sarah@barcelonacodeschool.com",
                        Firstname,
                        Lastname,
                        AddressLine1 ,
                        AddressLine2,
                        Postcode,
                        City,
                        Phonenumber,
                        text: 'Thanks for your order.',
                        amount: (this.props.amount),
                        orders: this.props.orders.map ((el,i)=>{
                            title: el.title
                            description: el.description
                            
                             
                            
                        }) 
                       
            
                    }

                    
                    
                   that.props.history.push({pathname:'/success', 
                   state : {
                     amount:this.props.amount,
                     Firstname:this.props.Firstname,
                     Lastname:this.props.Lastname,
                     AddressLine1:this.props.AddressLine1,
                     AddressLine2:this.props.AddressLine2,
                     Postcode:this.props.Postcode,
                     City:this.props.City,
                     Phonenumber:this.props.Phonenumber,
                     Email:this.props.Email
                    }
                  })
                                      
                  Meteor.call('send_email2', email, (err,id) =>{
                    debugger
                  })
                
                  Cart_api.remove({}, (e,d) => {
           
            
                    })
                

              }else if(data.type =='StripeInvalidRequestError'){
                   alert('hmmm ... StripeInvalidRequestError')
               }
              }
            }
        )
  
    }

  

    render() {

      
        let style ={ display:this.state.display || 'none', marginLeft: "5%"}
        let stripecheckout = {display: this.state.displayPayment, marginBottom: "20px",marginLeft: "24%"}
        let paymentbutton = {display: this.state.displayButton, backgroundColor :'#00A694', marginTop: "20px", marginBottom: "20px",marginLeft: "260px"}
        
        if(this.state.err) {
          return <h1>Hmmm ... something is not right</h1>
        }
          
        return (
        <div>
            <div style = {stripecheckout}> 
            <StripeCheckout
                token       =  {this.onToken}
                stripeKey   =  {secret.publishablekey}
                amount      =  {this.props.amount * 100}
                email       =  {this.props.email}
        
            />
          </div>
          <Button 
                    onClick = {this.paymentSubmit} 
                    variant="contained" color="secondary" style = {paymentbutton} >
                                    Continue to Payment
                    </Button>

           <div style ={style}> 
                <Message
                        
                            message = {this.state.message}
                            color   = {this.state.color}
                            marginBottom = {this.state.marginBottom}
                            colorText = {this.state.colorText}
                />
            </div>
        </div>

        )

     

  }
}
