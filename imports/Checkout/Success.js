import React from 'react'
import color from '@material-ui/core/colors/lime';
import { NavLink } from 'react-router-dom';
import Button from '@material-ui/core/Button';

export default class Success extends React.Component {
state= {
 amount :'',
Firstname:'',
Lastname: '',
AddressLine1:'',
AddressLine2:'',
Postcode:'',
City:'',
Phonenumber:'',
Email:''
}
    _renderTotal = () => {

    }
    componentDidMount(){
        var {
            amount,
            Firstname ,
            Lastname ,
            AddressLine1, 
            AddressLine2 , 
            Postcode ,
            City ,
            Phonenumber, 
            Email 
        } = this.props.location.state
        
        this.setState({Email, amount,
            Firstname,
            Lastname,
            AddressLine1,
            AddressLine2,
            Postcode,
            City,
            Phonenumber
        })
        // put this in the state
        // then display it in the screen!
    }

    render() {
        return(
            <div className = 'biggrid'>
            <div>
            <h1 style = {{marginTop:'100px', color:'green',marginLeft: '35px'}}>Order Successful!</h1>
            </div>
              <div className = 'griddy'>
                <div className = 'griddygrid' >
                <div className = 'fewmoregrids' style = {{marginLeft:'5%'}}>
        <h1 className = 'forthefont20'>Fullname:</h1> <span className = 'forthefont11' style =  {{marginTop:'8%',}}>{this.state.Firstname} {this.state.Lastname} </span>
                </div>
                <div className = 'fewmoregrids' style = {{marginLeft:'5%'}}>
                <h1 className = 'forthefont20' style = {{marginBottom: '0',marginTop: '6%'}}>Address:</h1>
                <span className = 'forthefont11' style ={{marginTop:'5%'}} >{this.state.City}</span>
                <div></div>
                <span className = 'forthefont11' >{this.state.Postcode}</span>
                <div></div>
                <span className = 'forthefont11' >{this.state.AddressLine1}</span>
                <div></div>
                <span className = 'forthefont11'>{this.state.AddressLine2}</span>
                
                </div>
                <div className = 'fewmoregrids' style = {{marginLeft:'5%'}}>
                <h1 className = 'forthefont20'>Contact Number:</h1>  <span className='doesnotwork' >{this.state.Phonenumber}</span>
                </div>
                <div className = 'fewmoregrids' style = {{marginLeft:'5%'}}>
                <h1 className = 'forthefont20'>E-mail Address:</h1> <span className = 'forthefont11' style = {{marginTop:'7%'}}>{this.state.Email}</span>
                </div>
                </div >
                <div className = 'griddygrid2'>
                <div className = 'fewmoregrids' style = {{marginLeft:'5%',marginTop:'35px',marginRight: '5%',}}>
                
                <h1 className = 'forthefont20'>Total:</h1>
                <span className = 'n' >{this.state.amount} €</span>
                
                </div>
                </div>
                <div>
                </div>
                <div className = 'buttons3'>
                    <NavLink to= {"/shop/"}>
                    <Button variant="contained" color="secondary" style = {{backgroundColor :'#00A694'}}>
                                Continue Shopping
                    </Button>
                    </NavLink>
            </div>
            </div>
            </div>
        )
    }


}