import React from 'react'
import Orders_list from './Orders_list';
import {Cart_api} from '../api/cart_api'
import { Products_api } from '../api/products_api';
import NavLink  from 'react-router-dom';
import Empty from '../Cart/Empty'
export default class Cart extends React.Component {
    state = {
        order: '', orders : [] , arr:[]
    }
    componentDidMount () {
        var arr = []
        Tracker.autorun(() => {
            arr = []
            var orders = Cart_api.find({}).fetch()
            
            orders.map((ele)=>{
               
                var myProduct = Products_api.findOne({_id:ele.productID})
                
                if(myProduct){
                    myProduct.qty = ele.qty
                    myProduct.order_id = ele._id
                    myProduct.selected = ele.selected
                    arr.push(myProduct)
                }
                
            })
        this.setState({arr},()=>{
            console.log('-===-',this.state)
        })
       
    })
}
    render() {
       
        if(this.state.arr.length > 0){
        return (
            <div>
            <div className = "backgroundimgCart"> 
                <h1 className = "title"> Cart </h1>
                
            </div>

             <Orders_list
                orders = {this.state.arr}
                remove = {this.props.remove}
                increaseQuantity = {this.props.increaseQuantity}
                decreaseQuantity = {this.props.decreaseQuantity}
                open = {this.props.open}
                text = {this.props.text}
                close = {this.props.close}
               
             />

             
           </div>
        )
    }else{
        return (
            <div>
            <div className = "backgroundimgCart2"> 
                <h1 className = "title"> Cart </h1>
            </div>
            <Empty/>

           </div>
    )
    }
    
    }
}