import React from 'react'
import { NavLink } from 'react-router-dom';
import Button from '@material-ui/core/Button';



export default class Empty extends React.Component {

    render() {
        
        return (
        <div>
        <div className = 'empty'>
        
        <h1 style ={{marginLeft:'3%'}} >Your cart is empty …</h1>
        
        
        
        </div>
        <div className = 'somegrid'>
        <div>
        </div>
        <div className = 'emptybutton'>
         <NavLink to= {"/shop/"}>
                    <Button variant="contained" color="secondary" style = {{backgroundColor :'#00A694',}}>
                                Continue Shopping
                    </Button>
                    </NavLink>
        </div>
        </div>
        </div>
        )

}

}