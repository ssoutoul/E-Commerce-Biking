import React from 'react';
import Button from '@material-ui/core/Button';
import { Cart_api }   from '../api/cart_api';
import Modal from 'react-responsive-modal';


export default class Orders_items extends React.Component {
      
    
    handleRemove = () => {
            
            this.props.remove(this.props.order.order_id)

    }



    render () {
        const { open } = this.props;
   
        return (
            
            <div>
                
                <div className = 'cartgrid'>
                      
                        <div>
                            <div className = 'cartbutton' onClick = {this.handleRemove}><button className="fas fa-times-circle"></button></div>
                            <img  className = 'cartimg'src={this.props.order.url}/>    
                         
                        </div>
                        
                        <div className = 'cartinfo'>
                            
                            <div className = 'biggraph'>
                                <span className = 'bigtitle'>{this.props.order.title}</span> 
                            </div>
                            <div className = 'lilgraph' style = {{borderBottom: "1px solid black"}}> 
                                <span className = 'info' >Price: </span>
                                <span className = 'value'>{this.props.order.price} €</span>
                            </div>
                            <div className = 'lilgraph' style = {{borderBottom: "1px solid black"}}>
                                <span className = 'info'>Size:</span>    
                                <span className = 'value'>{this.props.order.selected}</span>
                            </div>
                            <div className = 'lilgraph' style = {{borderBottom: "1px solid black"}}>
                                <span className = 'info'>Quantity: </span>  
                                <span className = 'value'><button onClick = {() => this.props.increaseQuantity(this.props.order.order_id)} className = "quantitybutton">+</button><button onClick = {() => this.props.decreaseQuantity(this.props.order.order_id)} className = "quantitybutton" style = {{marginRight: "5%"}}>-</button>{this.props.order.qty}</span>
                            </div>
                            <Modal  open={open} onClose =  {() => this.props.close()} center>
                                    <p>
                                    {this.props.text}
                                    </p>
                                </Modal>
                            <div className = 'lilgraph'>
                                <span className = 'info'>Subtotal:</span>
                                <span className = 'value'>{this.props.order.price * this.props.order.qty} €</span>
                            </div>
                           
                        </div>

                </div>
                
            </div>
        )
    }


}
