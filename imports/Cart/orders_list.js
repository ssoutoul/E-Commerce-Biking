import React from 'react';
import Button from '@material-ui/core/Button';
import Orders_items from './Orders_items'
import { NavLink } from 'react-router-dom';

export default class Orders_list extends React.Component {
   

    _renderTotal = () => {
        var total = 0
        this.props.orders.map ((el,i)=>{
            
            el.index = i
            total += (Number(el.price) * el.qty)
             
         }) 
        
         return total
    }

    _renderOrders = () => {
     
        return this.props.orders.map ((el,i)=>{
            
            
            return <Orders_items

                key = {i}
                order = {el}
                remove = {this.props.remove}
                increaseQuantity = {this.props.increaseQuantity}
                decreaseQuantity = {this.props.decreaseQuantity}
                open = {this.props.open}
                text = {this.props.text}
                close = {this.props.close}
                
                />
            
            
        }) 
    }
    

    render () {
        return (
            <div>
                <h1 className = 'cartTitle' > Order Details </h1>
                {this._renderOrders()}
                
                <h1 className = 'total' >{this._renderTotal()} €</h1>
                <p className = 'VAT' >Incl. VAT : 19%</p>
                <div className = 'buttons'>


                    <NavLink to= {"/checkout/"}>
                    <Button 
                    variant="contained" color="secondary" style = {{backgroundColor :'#00A694'}} >
                                    Check Out
                    </Button>
                    </NavLink>
                    </div>
                
                <div className = 'buttons'>
                    <NavLink to= {"/shop/"}>
                    <Button variant="contained" color="secondary" style = {{backgroundColor :'#00A694'}}>
                                Continue Shopping
                    </Button>
                    </NavLink>

                
                </div>
            </div>

        )
    }


}

