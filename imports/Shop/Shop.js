import React from 'react'
import Product_list from './Product_list'
// import { Products_api }   from '../../imports/api/products_api'
import {Products_api} from '../api/products_api'


export default class Shop extends React.Component  {

    state = {
        product: '', products:[]
    }

    componentDidMount () {
        Tracker.autorun(()=>{
            var products = Products_api.find().fetch()
            this.setState({products})
            
          
        }) 
    }


    removeProduct= id => {
        Meteor.call('removeProduct',id)
    }


    render () {
        
        return (
            <div>
               <div className = "backgroundimg"> 
                   <h1 className = "title"> Shop </h1>
               </div>
               
                <Product_list 
                    products = {this.state.products}
                    removeProduct = {this.removeProduct}
                    insertCart = {this.props.insertCart}
                    size = {this.props.size}
                    open = {this.props.open}
                    close = {this.props.close}
                    text = {this.props.text}
                    onOpenModal = {this.props.onOpenModal}
                    color= {this.props.color}
                   
                    
                />

                
                
            </div>
        )
    }
}