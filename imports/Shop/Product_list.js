import React from 'react'
import Product_item from './Product_item'

export default class Product_list extends React.Component {

    _renderProducts = () => {
        
        return this.props.products.map ((el,i)=>{
            el.index = i
            return <Product_item 
                key = {i}
                product = {el}
                removeProduct = {this.props.removeProduct}
                insertCart = {this.props.insertCart}
                size = {this.props.size}
                open = {this.props.open}
                close = {this.props.close}
                text = {this.props.text}
                color= {this.props.color}
                onOpenModal = {this.props.onOpenModal}
                />
        }) 
    }
   
    render () {
        return (
            <div className="container">
                {this._renderProducts()}
                
            </div>

        )
    }


}