import React from 'react'
import Button from '@material-ui/core/Button';
import { NavLink } from 'react-router-dom';
import {Products_api} from '../api/products_api'
import { Cart_api } from '../api/cart_api'
import Modal from 'react-responsive-modal';

export default class Product_item extends React.Component {
    state = {
        S:'none', 
        M:'none', 
        L:'none'
        
      
    }

    
     
    handleRemove = id => {
        this.props.removeProduct(this.props.product._id)
    }

    handleClick = (id,size) => {

        let x = 0;
        for (let key in this.state){
            if(this.state[key] != 'none'){
               
                x++
            }
        }
        if(x > 0){
          
            this.props.insertCart(id, size)
        }else {
            this.props.onOpenModal()
            
        }

        

    }

    
 
   

shortener = str => str.substr(0,160) + '...'
    
buttonClick = e => {
    this.props.size(e.target.name)
    
    for (var key in this.state){
        if(key != e.target.name && key != 'myProduct'){
            this.setState({[key]:'none'})
        }
} 
        this.state[e.target.name] == 'none'? 
        this.setState({[e.target.name]: 'grey'})
        : this.setState({[e.target.name]:'none'})
}

render () {
         const { open } = this.props;
         let modal = {
            background:this.props.color,
            
        }

        function createMarkup(stuff) { return {__html:stuff}; };
        return (
            <div className = "grid">
           {
               Meteor.userId()?
               <button 
               onClick = {this.handleRemove}
               className = "deletebutton" >X</button>:null 
            }
            <div>
            
            <NavLink to= {"/product/" + this.props.product._id}>
                <img className = "products-img" src = {this.props.product.url}/>
            </NavLink> 
                    
               
             
            
                <div style = {{textAlign: "left", marginLeft: "30px", fontFamily: 'Noto Sans'}}>
                <NavLink to= {"/product/" + this.props.product._id}>
                <p className = 'try' style = {{fontWeight: "bold"}}>{this.props.product.title}</p>
                </NavLink>
                <NavLink to= {"/product/" + this.props.product._id}>
                <div className = 'try2' dangerouslySetInnerHTML={createMarkup(this.shortener(this.props.product.description))} />
                </NavLink>
                <NavLink to= {"/product/" + this.props.product._id}>
                <h2 className = 'try2' style = {{marginBottom:'0', marginTop:'0'}}>{this.props.product.price} €</h2>
                </NavLink>
                <div>
                <div onClick = {this.buttonClick} style = {{marginBottom:'3%'}}>
                            <button 
                                
                                name = "S"
                                style ={{background:this.state.S, width: "20%",
                                height: "40px", marginTop: "4%"}}>S</button>
                            <button 
                                
                                name = "M"
                                style ={{background:this.state.M, width: "20%",
                                height: "40px"}}>M</button>
                            <button 
                                
                                name = "L"
                                style ={{background:this.state.L, width: "20%",
                                height: "40px"}}>L</button>
                    </div>
                <Button onClick = {()=>this.handleClick(this.props.product._id)} variant="contained"  color="primary" style = {{background: "#00A795"}}>

                    Add to Cart
                    <i className="fas fa-shopping-cart"></i></Button>
                <div style = {modal}>
                <Modal  open={open} onClose =  {() => this.props.close()} center>
                    <p>
                        {this.props.text}
                    </p>
                </Modal>
                </div>
                    
                </div>
                </div>
            </div>
        
            </div>
           
       
        )
    }



}