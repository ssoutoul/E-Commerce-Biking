import React from 'react';
import Button from '@material-ui/core/Button';
import Message from '../Message'
export default class Form extends React.Component{

state = {
 name : '',
 email : '',
 subject :'', 
 text :'',
 color:'', 
 message: '', 
 display: '', 
 colorText: '', 
 marginBottom:''
}


handleChangeName = (event) =>{
  this.setState({name: event.target.value},()=>{
    console.log(this.state)
  })
  
}
handleChangeEmail = (event) =>{
  this.setState({email: event.target.value},()=>{
    console.log(this.state)
  })
  
}
handleChangeSubject = (event) =>{
  this.setState({subject: event.target.value},()=>{
    console.log(this.state)
  })
  
}
handleChangeText = (event) =>{
  this.setState({text: event.target.value},()=>{
    console.log(this.state)
  })
  
}
banana = (event)=>{
  event.preventDefault()
  let email = {
    sender:this.state.email,
    receiver:'100104045n@gmail.com',
    name:this.state.name,
    subject:this.state.subject,
    content:this.state.text
}
Meteor.call('send_email', email, (err,id) =>{
      if (err){
        this.setState({
          color:'#f8d7da',
          message:'Email did not go through. Please try again',
          display:'block',
          colorText: "#721c24",
          marginBotttom: "30px"
      },()=>{
        setTimeout(()=>{
          this.setState({display:'none'})
        },5000)
      })
       
      }else{
        this.setState({
          color:'#d4edda',
          message:'Email sent.',
          display:'block',
          colorText: "#155724",
          marginBotttom: "30px"
      },()=>{
        setTimeout(()=>{
          this.setState({display:'none'})
        },5000)
      })
       
      }
      this.setState({
        name : '',
        email : '',
        subject :'', 
        text :''

      })
  
}


)}




  render(){
    let style ={ display:this.state.display || 'none' }
return(



  
<div className = 'newgrid' >

<div className="the">
<h1> Get in Touch</h1>
<p>Got a question? We’d love to hear from you!</p>

<form onSubmit={this.banana} 
  autoComplete={false}
>

  <input 
  required
      onChange = {this.handleChangeName}  
      className='somemoreinput'  
      type='text' name='Name' 
      autocomplete="off"
      placeholder ='Name' 
      value = {this.state.name}
    />
  <input 
  required 
  onChange = {this.handleChangeEmail} 
  className='somemoreinput'
   type='email'
    name='E-mail'
    autocomplete="off"
   placeholder ='E-mail'
   value = {this.state.email}
   />
  
  
  <input
  required
   onChange = {this.handleChangeSubject} 
   className='somemoreinput' 
   type='text' 
   name='Subject'
   autocomplete="off"
  placeholder ='Subject'
  value = {this.state.subject}
    />
  <textarea 
  required 
  onChange = {this.handleChangeText} 
  className='smallinput'  
  name='Message' 
  placeholder ='Message'
  value = {this.state.text}
  />


<div className ='buttonthing'>
<Button  type ="submit" variant="contained" color="secondary" style = {{background: "#00A795", width: "30%",float: "right",
marginRight: "24px",marginTop:"5%"}}>
        Submit
      </Button>
<div style ={style}> 
  <Message
    message = {this.state.message}
    color   = {this.state.color}
    marginBottom = {this.state.marginBottom}
    colorText = {this.state.colorText}
    display = {this.state.display}
   />
</div>
</div>
</form>
</div>


<div className='the'>
<h1 className = 'sizeofmap' >Location </h1>
<div className = 'video-container'>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2993.8753939563358!2d2.1487947154254514!3d41.37679187926506!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4a3202d6aeda5%3A0x92d1214178c0904c!2sRider+Boutique+Barcelona!5e0!3m2!1sen!2ses!4v1530453359970"  frameBorder="0" style= {{border:0}} allowFullScreen></iframe>

    </div>
</div>


</div>

)
  }
}
