import React from 'react'

export default class UploadImages extends React.Component{
        uploadWidget=  () =>{
            var that = this
        cloudinary.openUploadWidget({ 
                cloud_name: 'ssoutoul', 
                upload_preset: 'banana', 
                tags:["product"],
                stylesheet: '#cloudinary-overlay.modal{background:white; !important} div#cloudinary-widget  {width:50% !important; left:25% !important; background:white;} ul.sources.clear{display:flex;} .widget .panel.local .drag_area{background:white} @media screen and (max-width:600px){div#cloudinary-widget  {width:100% !important; left:0 !important;}'	
        },
            function(error, result) {
                if(error){
                  //handle error
                //   
                  alert("The picture hasn't been uploaded")
                }else{
                //add the picture to your database in its own collection
                // 
                 that.props.images(result[0].url, result[0].public_id) 
                 alert("Picture successfully uploaded!")
                
                }
            });
    }

        render(){
                return (
                    <div className="main">
                        <div className="upload">
                            <button className ="ui button"
                                    onClick={this.uploadWidget} >
                                add image
                            </button>
                        </div>
                </div>
                )
        }
}
