import React from 'react'

export default class Admin extends React.Component  {

    state = {
        email:'',
        password:''
    }

    handleChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    // handleSubmit = e => {
    //     e.preventDefault()
    //     let { email, password } = this.state;
    //     Meteor.call('createUser',{email, password}, (err, user)=>{
    //         
    //     })
    // }
    handleSubmit = e => {
        e.preventDefault()

        let { email, password } = this.state;
        Meteor.loginWithPassword({email}, password, (err,data)=>{
            if(Meteor.userId()){
                this.props.history.push('/Admin')
            }else{
                alert(err.reason)
            }
        })
    }

     
    render () {

        return (
            <div className = "logincontainer">
                <img className = 'loginimg' src='http://res.cloudinary.com/ssoutoul/image/upload/v1530104876/logo.png'/>
                <div className = "apple">
                    <div className = "loginbox">
                        <h1>Welcome Admin</h1>
                        <form 
                            onSubmit ={this.handleSubmit}
                            onChange ={this.handleChange}>
                            <input className = "loginemail" type = "text" name ="email" placeholder = "Email"/>
                            <input className ="loginpassword" type="text" name = "password" placeholder = "Password"/>
                            <button> Login </button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
    
    
}
