import React from 'react'
import Button from '@material-ui/core/Button';
import UploadImages from './UploadImages'

export default class AdminAccess extends React.Component  {

    state = {
       title: '',
       description: '',
       price: '',
       url: '',
       public_id: ''

    }

    // addCommas = (nStr) =>{
    //     debugger
    //     nStr += '';
    //     var x = nStr.split('.');
    //     var x1 = x[0];
    //     var x2 = x.length > 1 ? '.' + x[1] : '';
    //     var rgx = /(\d+)(\d{3})/;
    //     while (rgx.test(x1)) {
    //      x1 = x1.replace(rgx, '$1' + ',' + '$2');
    //     }
    //     return x1 + x2;
    // }

    nameChange = e => {
        this.setState({[e.target.name]: e.target.value})
        
    }

    nameSubmit = e => {
        var self = this
       // 
       e.preventDefault()
       for (var key in this.state) {
            if(this.state[key] == '' ) {
                return alert('Fill all required fields!!!')
            }
    } 
       
       Meteor.call('addProduct', {...this.state},(err,id)=>{
                if (err) {
                    alert("The product hasn't been added")
                } else {
                    alert("The product has been successfully added")
                }
                
                self.setState({       
                    title: '',
                    description: '',
                    price: '',
                    quantity: '',
                    url: '',
                    public_id: ''
            })
            })
    }

    _uploadImages = (url,public_id) => {
        // 
        this.setState({url: url, public_id: public_id})
        
    }

    // _clearInput = e => {
    //     this.setState({[e.target.name]: ''})
    // }

     componentWillMount(){
         !Meteor.userId() ? this.props.history.push('/login'): null
     }
    render () {

        return (
            <div className = "form">
                
                <div className = "backgroundimg"> 
                    <h1 className = "title"> Admin </h1>
                </div>
                <div className = "addform"
                >
                    <h1>Title </h1>
                    <input  
                    required
                    onChange = {this.nameChange} value = {this.state.title}  type = "text" name ="title" placeholder = "Title"/>
                    <h1>Description </h1>
                    <textarea  
                    required
                    onChange = {this.nameChange}  value = {this.state.description} type = "text" name ="description" placeholder = "Description"/>
                    <h1>Price</h1>
                    <input  
                    required
                    onChange = {this.nameChange}  value = {this.state.price} type = "number" name ="price" placeholder = "Price"/>
                    <h1>Quantity</h1>
                    <input  
                    required
                    onChange = {this.nameChange}  value = {this.state.quantity} type = "number" name ="quantity" placeholder = "Quantity"/>
                    <h1>Upload Image </h1>
                    <UploadImages images = {this._uploadImages}/>
                    <Button 
                    onClick = {this.nameSubmit}
                    // onClick = {this._clearInput}
                    type ="submit" variant="contained" color="primary" style = {{background: "#00A795", width: "10%", marginTop: "5%"}} >
                    Add</Button>
                </div>
               

                    <Button  variant="contained" color="primary" style = {{background: "#00A795", width: "10%",marginLeft: "5%"}}  
                    onClick ={()=> Meteor.logout(this.props.history.push('/login'))}>
                    Logout</Button>

               
            </div>
        )
    }
    
    
}
