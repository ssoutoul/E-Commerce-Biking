import React from 'react'
import Button from '@material-ui/core/Button';
import classnames from 'classnames';
import Modal from 'react-responsive-modal';

export default class Products extends React.Component {

    state = {
      S:'none', 
      M:'none', 
      L:'none'
      
    }
    componentDidMount () {
       this.props._renderProduct(this.props.match.params.id)
    }
    handleClick2 = (id,size) => {
        let x = 0;
        for (let key in this.state){
            if(this.state[key] != 'none'){
                
                x++
            }
        }
        if(x > 0){
            
            this.props.insertCart(id, size)
        }else {
           this.props.onOpenModal()
            
        }
    }

    handleClick = e => {
        
        this.props.size(e.target.name)
        

        for (var key in this.state){
            if(key != e.target.name && key != 'myProduct'){
                this.setState({[key]:'none'})
            }
        }
        this.state[e.target.name] == 'none'? 
        this.setState({[e.target.name]: 'grey'})
        : this.setState({[e.target.name]:'none'})
      }

    render () {
        const { open } = this.props;
        function createMarkup(stuff) { return {__html:stuff}; };

      
        let { product } = this.props
        let classes = classnames('specialbutton', {active: this.state.active});
        if(product){
            
            return (

            <div>
                <button className= "backbutton"><i class="fas fa-chevron-left"></i>Back</button>
                <div className = "productcontainer">
                    <img className = "productimage" src = {product.url}/>
                    <div>
                        <h2 className = "productname" >{product.title}</h2>
                        <div className = "productdescription" dangerouslySetInnerHTML={createMarkup(product.description)} />
                        <div onClick = {this.handleClick} className= "buttongallery">
                            <button 
                                name = "S"
                                style ={{background:this.state.S, width: "10%",
                                    height: "50px", marginLeft: "5%"}}>S</button>
                            <button 
                                name = "M"
                                style ={{background:this.state.M, width: "10%",
                                height: "50px"}}>M</button>
                            <button 
                                name = "L"
                                style ={{background:this.state.L, width: "10%",
                                height: "50px"}}>L</button>
                        </div>
                       
                
                        <div className= "productgrid">
                            <p className = "productprice" >{product.price} €</p>

                            <button className = "productbutton" onClick =  {()=>this.handleClick2(this.props.product._id)}>Add to Cart<i className="fas fa-shopping-cart"></i></button>

                        
                        </div>
                        <Modal open={open} onClose =  {() => this.props.close()} center>
                            <p>{this.props.text}</p>
                        </Modal>
                    
                     
                    </div>
                         
                </div>


            </div>

        )
    }else return null
    }

}