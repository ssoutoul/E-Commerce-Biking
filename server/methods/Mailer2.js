import { Email } from 'meteor/email'
import {Meteor} from 'meteor/meteor'

Meteor.methods({
    'send_email2':function(options){
        console.log('===========o p t i o n s ==============', options)
        Email.send({ 
            to: [options.receiver, options.receiver2],
            name: options.Firstname,
            last: options.Lastname,
            address: options.AddressLine1,
            address2: options.AddressLine2,
            city: options.City,
            postcode: options.Postcode,
            number: options.Number,
            html: `<div> ${options.text}</div> <div>Your total is of ${options.amount} €</div>
        
            <div>You order will be shipped to</div> 
            <div> ${options.City}</div>  
            <div>${options.Postcode}</div>
            <div>${options.AddressLine1}</div> 
            <div>${options.AddressLine2}</div>
            <div>Thank you for your oreder </div>
            <div>${options.Firstname} ${options.Lastname}.</div>`
        })
    }
})