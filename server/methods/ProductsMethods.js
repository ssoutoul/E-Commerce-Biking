import { Products_api }   from '../../imports/api/products_api'
import { Meteor }  from 'meteor/meteor';

Meteor.methods({
    addProduct:function(obj){
       
            var ProductId  = Products_api.insert(obj)
            return ProductId;
    },
    removeProduct:function(id) {
        if( this.userId){
           // delete
           Products_api.remove({_id:id})
        }
    }
   
})