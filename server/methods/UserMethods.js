import { Meteor } from 'meteor/meteor'
import { Accounts } from 'meteor/accounts-base'

Meteor.methods({
    'addUser' :function (email, password) {
       var myUser =  Accounts.createUser({
           email, password});
       return myUser
    }
})