import { Email } from 'meteor/email'
import {Meteor} from 'meteor/meteor'

Meteor.methods({
    'send_email':function(options){
        console.log('===========o p t i o n s ==============', options)
        Email.send({ 
            to: options.receiver, 
            from: options.sender,
            name: options.name,
            subject: options.subject,
            html: `<div>Email sent from: ${options.sender}</div> 
            <div> Sender name : ${options.name} </div>
             <div>${options.content}</div>`
        })
    }
})