import  { Meteor }  from 'meteor/meteor'
import { secret } from '../../secret'
var Stripe = StripeAPI(secret.secretkey);

var charge = (token, amount)=>{
    var myCharge = Stripe.charges.create({
          source: token.id,
          amount: amount,
          currency: 'eur',
          receipt_email: secret.myEmail,
      })
      return myCharge
    }
    
    
    Meteor.methods({
      StripeChargeMethod: function(token, amount){
        try{
          var data = charge(token, amount)
          console.log('*****************************************************************************success*****************************************************************************************************?')
          return data
        }catch(error){
          console.log('*****************************************************************************error*******************************************************************************************************?')
          return error
        }
      }
    })
    